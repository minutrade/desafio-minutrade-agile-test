process.title = 'challenge-qa-minutrade';
const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./lib/routes');

const API_PORT = 5877;
const app = express();

app.use(bodyParser.json());
app.use(routes);

app.listen(API_PORT, (err) => {
  if (err) {
    console.error('Error on listen port ', API_PORT);
    process.exit(1);
  } else {
    console.log('Server listening on port ', API_PORT);
  }
});

module.exports = app;
