const postDebit = require('./post-debit-controller');
const postCredit = require('./post-credit-controller');
const getAccountBalance = require('./get-balance-controller');
const account = require('./account-controller');

module.exports = {
  credit: {
    post: postCredit,
  },
  debit: {
    post: postDebit,
  },
  account: {
    create: account.create,
    validate: account.validate,
    balance: getAccountBalance,
  },
};
