const accounts = require('./db.json').accounts;

const findAccount = (code, callback) => {
  const account = accounts.find(acc => acc.code === code);
  return process.nextTick(callback, null, account);
};

const createAccount = (code, name, callback) => {
  findAccount(code, (err, account) => {
    if (account) {
      return process.nextTick(callback, 'Account already exists');
    }
    const newAccount = {
      code,
      name,
      balance: 0,
    };

    accounts.push(newAccount);
    return process.nextTick(callback, null, newAccount);
  });
};

const debitAccount = (code, value, callback) => {
  findAccount(code, (err, res) => {
    const account = res;
    if (res) {
      account.balance -= value;
      return process.nextTick(callback, null, account);
    }
    return process.nextTick(callback, 'Invalid account');
  });
};

const creditAccount = (code, value, callback) => {
  findAccount(code, (err, res) => {
    const account = res;
    if (res) {
      account.balance += value;
      return process.nextTick(callback, null, account);
    }
    return process.nextTick(callback, 'Invalid account');
  });
};

module.exports = {
  creditAccount,
  debitAccount,
  findAccount,
  createAccount,
};
