const express = require('express');
const controllers = require('./controllers');

const router = new express.Router();

router.post('/accounts', controllers.account.create);

router.post('/accounts/:code/debit', controllers.account.validate, controllers.debit.post);
router.post('/accounts/:code/credit', controllers.account.validate, controllers.credit.post);

router.get('/accounts/:code/balance', controllers.account.validate, controllers.account.balance);

router.all('*', (req, res) => {
  res.status(404).json({ message: 'Recurso não encontrado!' });
});

module.exports = router;
